// Copyright 2021 The Xcb-Go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build ignore
// +build ignore

package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"

	"modernc.org/ccgo/v3/lib"
)

const (
	tarFile = tarName + ".tar.gz"
	tarName = "libxcb-1.14"
)

type supportedKey = struct{ os, arch string }

var (
	gcc       = ccgo.Env("GO_GENERATE_CC", ccgo.Env("CC", "gcc"))
	gxx       = ccgo.Env("GO_GENERATE_CXX", ccgo.Env("CXX", "g++"))
	goarch    = ccgo.Env("TARGET_GOARCH", runtime.GOARCH)
	goos      = ccgo.Env("TARGET_GOOS", runtime.GOOS)
	supported = map[supportedKey]struct{}{
		{"darwin", "arm64"}: {},
		{"linux", "386"}:    {},
		{"linux", "amd64"}:  {},
		{"linux", "arm"}:    {},
		{"linux", "arm64"}:  {},
	}
	tmpDir           = ccgo.Env("GO_GENERATE_TMPDIR", "")
	verboseCompiledb = ccgo.Env("GO_GENERATE_VERBOSE", "") == "1"
)

func main() {
	fmt.Printf("Running on %s/%s.\n", runtime.GOOS, runtime.GOARCH)
	if _, ok := supported[supportedKey{goos, goarch}]; !ok {
		ccgo.Fatalf(true, "unsupported target: %s/%s", goos, goarch)
	}

	ccgo.MustMkdirs(true,
		"lib",
	)
	if tmpDir == "" {
		tmpDir = ccgo.MustTempDir(true, "", "go-generate-")
		defer os.RemoveAll(tmpDir)
	}
	ccgo.MustUntarFile(true, filepath.Join(tmpDir), tarFile, nil)
	cdb, err := filepath.Abs(filepath.Join(tmpDir, "cdb.json"))
	if err != nil {
		ccgo.Fatal(true, err)
	}

	cc, err := exec.LookPath(gcc)
	if err != nil {
		ccgo.Fatal(true, err)
	}

	cxx, err := exec.LookPath(gxx)
	if err != nil {
		ccgo.Fatal(true, err)
	}

	os.Setenv("CC", cc)
	os.Setenv("CXX", cxx)
	cfg := []string{
		"--enable-shared=no",
		"--enable-devel-docs=no",
		"--enable-ge",
		"--enable-xevie",
		"--enable-xprint",
	}
	make := "make"
	switch goos {
	case "darwin":
		make = "gmake"
	}
	if _, err := os.Stat(cdb); err != nil {
		if !os.IsNotExist(err) {
			ccgo.Fatal(true, err)
		}

		ccgo.MustInDir(true, filepath.Join(tmpDir, tarName), func() error {
			ccgo.MustShell(true, "./configure", cfg...)
			switch {
			case verboseCompiledb:
				ccgo.MustRun(true, "-verbose-compiledb", "-compiledb", cdb, make, "clean", "all")
			default:
				ccgo.MustRun(true, "-compiledb", cdb, make, "clean", "all")
			}
			return nil
		})
	}
	ccgo.MustRun(true,
		"-export-defines", "",
		"-export-enums", "",
		"-export-externs", "X",
		"-export-fields", "F",
		"-export-structs", "",
		"-export-typedefs", "",
		"-lmodernc.org/xau/lib",
		"-lmodernc.org/xdmcp/lib",
		"-o", filepath.Join("lib", fmt.Sprintf("xcb_%s_%s.go", goos, goarch)),
		"-pkgname", "xcb",
		"-trace-translation-units",
		cdb,
		".libs/libxcb-composite.a",
		".libs/libxcb-damage.a",
		".libs/libxcb-dpms.a",
		".libs/libxcb-dri2.a",
		".libs/libxcb-dri3.a",
		".libs/libxcb-ge.a",
		".libs/libxcb-glx.a",
		".libs/libxcb-present.a",
		".libs/libxcb-randr.a",
		".libs/libxcb-record.a",
		".libs/libxcb-render.a",
		".libs/libxcb-res.a",
		".libs/libxcb-screensaver.a",
		".libs/libxcb-shape.a",
		".libs/libxcb-shm.a",
		".libs/libxcb-sync.a",
		".libs/libxcb-xevie.a",
		".libs/libxcb-xf86dri.a",
		".libs/libxcb-xfixes.a",
		".libs/libxcb-xinerama.a",
		".libs/libxcb-xinput.a",
		".libs/libxcb-xkb.a",
		".libs/libxcb-xprint.a",
		".libs/libxcb-xtest.a",
		".libs/libxcb-xv.a",
		".libs/libxcb-xvmc.a",
		".libs/libxcb.a",
	)
	if err := filepath.Walk("examples", func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		if !strings.HasSuffix(path, ".c") {
			return nil
		}

		out := path[:len(path)-len(".c")]
		ccgo.MustRun(true,
			"-export-fields", "F",
			"-lmodernc.org/xcb/lib",
			"-I/opt/homebrew/Cellar/libxcb/1.14_1/include",
			"-o", fmt.Sprintf("%s_%s_%s.go", out, goos, goarch),
			path,
		)
		return nil
	}); err != nil {
		ccgo.Fatal(true, err)
	}
}
