# xcb

CGo-free port of libxcb, the interface to the X Window System protocol.


## Installation

    $ go get modernc.org/xcb

## Linking using ccgo

    $ ccgo foo.c -lmodernc.org/xcb/lib

## Documentation

[godoc.org/modernc.org/xcb](http://godoc.org/modernc.org/xcb)

## Builders

[modern-c.appspot.com/-/builder/?importpath=modernc.org%2fxcb](https://modern-c.appspot.com/-/builder/?importpath=modernc.org%2fxcb)
